package com.makeitrbreakit.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.UUID;
import java.util.stream.Stream;

import static java.lang.System.currentTimeMillis;
import static java.util.Optional.ofNullable;

@WebListener
@Component
public class MDCInterceptor extends HandlerInterceptorAdapter implements ServletRequestListener {

    private static final Logger logger = LoggerFactory.getLogger(MDCInterceptor.class);

    private static final String REQUEST_PRE_HANDLED = "requestPreHandled";
    private static final String REQUEST_POST_HANDLED = "requestPostHandled";

    private static final String REQUEST_ID = "requestId";
    private static final String REQUEST_START = "requestStart";
    private static final String REQUEST_URI = "uri";
    private static final String USER_ID = "userId";
    private static final String SOURCE_IP = "sourceIp";
    private static final String HEALTH_CHECK_USER = "health-check";

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) {
        if(request.getAttribute(REQUEST_PRE_HANDLED) == null) {
            final String requestId = UUID.randomUUID().toString();
            final String sourceIp = ofNullable(request.getHeader("X-FORWARDED-FOR"))
                    .filter(StringUtils::hasText)
                    .orElse(request.getRemoteAddr());
            request.setAttribute(REQUEST_PRE_HANDLED, true);
            request.setAttribute(REQUEST_ID, requestId);
            request.setAttribute(REQUEST_START, currentTimeMillis());
            MDC.put(REQUEST_ID, requestId);
            MDC.put(USER_ID, getUserId());
            MDC.put(REQUEST_URI, request.getRequestURI() + getRequestQuery(request));
            MDC.put(SOURCE_IP, sourceIp);
            this.replaceNullMdcValues();
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception ex) {
        if(request.getAttribute(REQUEST_POST_HANDLED) == null && !isHealthCheck()) {
            ofNullable(request.getAttribute(REQUEST_START))
                    .map(Long.class::cast)
                    .ifPresent(start -> logger.info("Request complete in [{} ms]", currentTimeMillis() - start));
        }
        request.setAttribute(REQUEST_POST_HANDLED, true);
    }

    private String getRequestQuery(HttpServletRequest request) {
        return ofNullable(request.getQueryString())
                .filter(StringUtils::hasText)
                .map(q -> "?" + MDCInterceptor.decode(q))
                .orElse("");
    }

    private static String decode(String value) {
        try {
            return URLDecoder.decode(value, "UTF-8");
        } catch(Exception e) {
            logger.warn("Failed to decode request query: " + value, e);
            return null;
        }
    }

    private boolean isHealthCheck() {
        return HEALTH_CHECK_USER.equals(MDC.get(USER_ID));
    }

    private String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication instanceof AnonymousAuthenticationToken) {
            return HEALTH_CHECK_USER;
        }
        return ofNullable(authentication)
                .map(Authentication::getPrincipal)
                .map(Object::toString)
                .orElse(null);
    }

    private void replaceNullMdcValues() {
        Stream.of(REQUEST_ID, USER_ID, REQUEST_URI, SOURCE_IP)
                .filter(key -> MDC.get(key) == null)
                .forEach(key -> MDC.put(key, "N/A"));
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        MDC.clear();
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {

    }

}
