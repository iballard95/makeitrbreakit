package com.makeitrbreakit.domain.stats;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class StatsServiceImpl implements StatsService {

    private StatsRepository statsRepository;

    @Override
    public SearchResult findAllStats(Integer numDays) {
        List<Stats> stats = statsRepository
                .findAllStatsByUsername(
                        SecurityContextHolder.getContext().getAuthentication().getName());
        List<String> dateList = new ArrayList<>();
        List<Integer> countList = new ArrayList<>();
        for (int i = 0; i < numDays; i++) {
            dateList.add(0, LocalDate.now().minusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            countList.add(0);
        }
        Map<String, List<Integer>> dateCountMap = new HashMap<>();
        stats.forEach(s -> {
            List<Integer> counts = new ArrayList<>(countList);
            dateCountMap.put(s.getDescription(), counts);
            counts.set(dateList.indexOf(s.getDate()), s.getCount());
        });
        List<StatsContext> statsContexts = new ArrayList<>();
        dateCountMap.forEach((k, v) ->
                statsContexts.add(new StatsContext(k, v)));
        SearchResult searchResult = new SearchResult();
        searchResult.setContexts(statsContexts);
        searchResult.setLabels(dateList);
        return searchResult;
    }

}

