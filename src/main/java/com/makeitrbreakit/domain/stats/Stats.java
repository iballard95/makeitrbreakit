package com.makeitrbreakit.domain.stats;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(StatsId.class)
@Data
public class Stats {

    @Id
    @Column
    private String description;

    @Id
    @Column
    private String date;

    @Column
    private Integer count;

}
