package com.makeitrbreakit.domain.stats;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatsId implements Serializable {

    private String description;
    private String date;

}

