package com.makeitrbreakit.domain.stats;

import lombok.Data;

import java.util.List;

@Data
public class SearchResult {

    private List<String> labels;
    private List<StatsContext> contexts;

}
