package com.makeitrbreakit.domain.stats;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class StatsContext {

    private String name;
    private List<Integer> counts;

}
