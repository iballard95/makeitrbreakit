package com.makeitrbreakit.domain.stats;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("stats")
@AllArgsConstructor
public class StatsController {

    private StatsService statsService;

    @GetMapping
    public SearchResult findAllStats(@RequestParam Integer numDays) {
        return statsService.findAllStats(numDays);
    }

}
