package com.makeitrbreakit.domain.stats;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatsRepository extends CrudRepository<Stats, String> {

    @Query(nativeQuery = true, value =
            "select h.description, DATE(s.created_datetime) as date, count(*) as count " +
                    "from habit as h " +
                    "join streak as s " +
                    "on h.id = s.habit_id " +
                    "where h.created_by = :name " +
                    "group by date, h.description " +
                    "order by h.description, date;")
    List<Stats> findAllStatsByUsername(@Param("name") String name);

}
