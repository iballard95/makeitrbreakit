package com.makeitrbreakit.domain.stats;

public interface StatsService {
    SearchResult findAllStats(Integer numDays);
}
