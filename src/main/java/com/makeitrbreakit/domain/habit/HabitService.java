package com.makeitrbreakit.domain.habit;

import java.util.List;

public interface HabitService {

    List<Habit> findAllByCreatedBy(String username);
    Habit update(Habit current, Habit incoming);
    Habit save(Habit habit);
    void delete(Habit habit);

}
