package com.makeitrbreakit.domain.habit;

import com.makeitrbreakit.domain.AbstractEntity;
import com.makeitrbreakit.domain.habit.streak.Streak;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Habit extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String description;

    @Column
    private Boolean completed = Boolean.FALSE;

    @Column
    private Integer hours = 0;

    @Column
    private Integer minutes = 0;

    @Column
    private Integer seconds = 0;

    @OneToMany(mappedBy = "habit", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Streak> streaks = new ArrayList<>();

    private Integer streakCount = 0;

    private Boolean started = Boolean.FALSE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Habit habit = (Habit) o;
        return id.equals(habit.id) &&
                description.equals(habit.description) &&
                completed.equals(habit.completed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, completed);
    }

    public Habit addStreak(Streak streak){
        streak.setHabit(this);
        streaks.add(streak);
        return this;
    }

}
