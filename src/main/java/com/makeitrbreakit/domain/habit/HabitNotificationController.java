package com.makeitrbreakit.domain.habit;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/timer/start")
@AllArgsConstructor
public class HabitNotificationController {

    private SimpMessagingTemplate messagingTemplate;

    @PostMapping
    public ResponseEntity habitTimer(@RequestBody final Habit habit) {
        if (habit.getStarted()) {
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            Runnable runnableTask = () -> {
                try {
                    long hoursAsMillis = habit.getHours() * 60 * 60 * 1000;
                    long minutesAsMillis = habit.getMinutes() * 60 * 1000;
                    long secondsAsMillis = habit.getSeconds() * 1000;
                    TimeUnit.MILLISECONDS.sleep(hoursAsMillis + minutesAsMillis + secondsAsMillis);
                    messagingTemplate.convertAndSendToUser(
                            habit.getCreatedBy(),
                            "/queue/notify",
                            habit
                    );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            executorService.execute(runnableTask);
        }
        return ResponseEntity.ok(habit);
    }

}
