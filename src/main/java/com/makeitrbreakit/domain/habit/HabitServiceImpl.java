package com.makeitrbreakit.domain.habit;

import com.makeitrbreakit.domain.habit.streak.Streak;
import com.makeitrbreakit.domain.habit.streak.StreakRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class HabitServiceImpl implements HabitService {

    private HabitRepository habitRepository;
    private StreakRepository streakRepository;

    @Override
    public List<Habit> findAllByCreatedBy(String username) {
        List<Habit> habits = habitRepository.findAllByCreatedBy(username);
        for (Habit habit : habits) {
            ZonedDateTime dateTimeEnd = ZonedDateTime.now();
            ZonedDateTime dateTimeStart = dateTimeEnd.minusDays(1);
            List<Streak> streaks = streakRepository.findDistinctByCreatedDatetimeBetween(dateTimeStart, dateTimeEnd);
            if (!streaks.stream().findAny().isPresent()) {
                habit.setStreakCount(0);
            }
        }
        return habits;
    }

    @Override
    public Habit update(Habit current, Habit incoming) {
        return save(current.copyFields(incoming, "streaks", "streakCount"));
    }

    @Override
    @Transactional
    public Habit save(Habit habit) {
        if (habit.getCompleted()) {
            Streak streak = new Streak();
            streak.setHabit(habit);
            habit.getStreaks().add(streak);
            streakRepository.save(streak);
        }
        return habitRepository.save(habit);
    }

    @Override
    public void delete(Habit habit) {
        habitRepository.delete(habit);
    }
}
