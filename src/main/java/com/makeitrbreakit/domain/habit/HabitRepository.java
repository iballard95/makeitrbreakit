package com.makeitrbreakit.domain.habit;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HabitRepository extends CrudRepository<Habit, Long> {
    List<Habit> findAll();
    List<Habit> findAllByCreatedBy(String username);
    Optional<Habit> findById(Long id);

}
