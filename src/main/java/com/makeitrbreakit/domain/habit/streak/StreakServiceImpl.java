package com.makeitrbreakit.domain.habit.streak;

import com.makeitrbreakit.domain.habit.Habit;
import com.makeitrbreakit.domain.habit.HabitRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class StreakServiceImpl implements StreakService {

    private StreakRepository streakRepository;
    private HabitRepository habitRepository;

    @Override
    @Transactional
    public Habit save(Habit current, Habit incoming) {
        Streak streak = new Streak();
        if (incoming.getCompleted()) {
            current.addStreak(streak);
            current.setCompleted(incoming.getCompleted());
            current.setStreakCount(current.getStreakCount() + 1);
            streakRepository.save(streak);
            current = habitRepository.save(current);
        }
        return current;
    }

}
