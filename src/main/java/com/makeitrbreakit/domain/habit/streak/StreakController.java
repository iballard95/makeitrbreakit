package com.makeitrbreakit.domain.habit.streak;

import com.makeitrbreakit.domain.habit.Habit;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/habits/{habitId}/streak")
@AllArgsConstructor
public class StreakController {

    private StreakService streakService;

    @PostMapping
    public ResponseEntity<Habit> save(@PathVariable("habitId") Optional<Habit> current, @RequestBody Habit incoming){
        return current.map(t -> new ResponseEntity<>(streakService.save(t, incoming), HttpStatus.CREATED))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
