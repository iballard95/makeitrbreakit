package com.makeitrbreakit.domain.habit.streak;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.makeitrbreakit.domain.AbstractEntity;
import com.makeitrbreakit.domain.habit.Habit;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
public class Streak extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private Habit habit;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Streak streak = (Streak) o;
        return id.equals(streak.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
