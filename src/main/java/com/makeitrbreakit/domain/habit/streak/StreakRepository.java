package com.makeitrbreakit.domain.habit.streak;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface StreakRepository extends CrudRepository<Streak, Long> {
    List<Streak> findDistinctByCreatedDatetimeBetween(ZonedDateTime dateTimeStart, ZonedDateTime dateTimeEnd);
}
