package com.makeitrbreakit.domain.habit.streak;

import com.makeitrbreakit.domain.habit.Habit;

public interface StreakService {
    Habit save(Habit current, Habit incoming);
}
