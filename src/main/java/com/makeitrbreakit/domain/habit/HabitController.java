package com.makeitrbreakit.domain.habit;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("habits")
@AllArgsConstructor
public class HabitController {

    private HabitService habitService;

    @GetMapping
    public List<Habit> findAllHabits() {
        return habitService.findAllByCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable("id") Optional<Habit> current) {
        if(current.isPresent()) {
            return new ResponseEntity<>(current, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity createHabit(@RequestBody Habit habit) {
        return new ResponseEntity<>(habitService.save(habit), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Habit> updateHabit(@PathVariable("id") Optional<Habit> current, @RequestBody Habit incoming){
        if (current.isPresent()){
            habitService.update(current.get(), incoming);
            return ResponseEntity.ok(current.get());
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteHabit(@PathVariable("id") Optional<Habit> before) {
        if( before.isPresent() ){
            habitService.delete(before.get());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
