package com.makeitrbreakit.audit.context;

import com.makeitrbreakit.domain.user.User;
import com.makeitrbreakit.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityUserContextService implements UserContextService {

    private UserRepository userRepository;

    @Autowired
    public SpringSecurityUserContextService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser() {
        Optional<User> u = userRepository.findById(getCurrentUserId());
        if( !u.isPresent() ){
            throw new UnauthenticatedException();
        }else{
            return u.get();
        }
    }

    @Override
    public Long getCurrentUserId() {
        Optional<User> user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if( !user.isPresent() ){
            throw new UsernameNotFoundException(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return user.get().getId();
    }

    @Override
    public String getCurrentUsername() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if( username == null ){
            throw new UnauthenticatedException();
        }else{
            return username;
        }
    }

}
