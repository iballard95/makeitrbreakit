package com.makeitrbreakit.audit.context;

import java.time.ZonedDateTime;

public interface DateTimeService {

    ZonedDateTime getCurrentDateTime();

}
