package com.makeitrbreakit.audit.context;

import com.makeitrbreakit.domain.user.User;

public interface UserContextService {

    User getCurrentUser();
    Long getCurrentUserId();
    String getCurrentUsername();

}
