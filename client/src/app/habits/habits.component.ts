import {Component, OnDestroy, OnInit} from '@angular/core';
import {HabitService} from '../shared/habit/habit.service';
import {Habit} from '../shared/habit/habit.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {StreakService} from '../shared/streak/streak.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Time} from '../shared/habit/time/time.module';
import {AuthService} from '../auth/AuthService';
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-habits',
  templateUrl: './habits.component.html',
  styleUrls: ['./habits.component.css'],
  providers: [NgbModal]
})
export class HabitsComponent implements OnInit, OnDestroy {
  habit: Habit = new Habit();
  public habits: Array<Habit>;
  subscription;
  public user;
  constructor(public habitService: HabitService,
              private auth: AuthService,
              private modalService: NgbModal,
              private toastr: ToastrService,
              private streakService: StreakService) {
    this.user = this.auth.user;
    this.connect();
  }

  ws: any;
  name: string;
  disabled: boolean;
  baseUrl = environment.apiHost;
  public hours: Array<Time> = new Array<Time>();
  public minutes: Array<Time> = new Array<Time>();
  public seconds: Array<Time> = new Array<Time>();

  connect() {
    const ws = new SockJS(this.baseUrl + '/socket');
    this.ws = Stomp.over(ws);
    const that = this;
    this.ws.connect({}, function (frame) {
      that.ws.subscribe('/errors', function (message) {
        that.toastr.error('Error occurred while running timer: ' + message.body, 'Habit');
      });
      that.ws.subscribe('/user/queue/notify', function (response) {
        const habit = JSON.parse(response.body);
        if (that.user.name === habit.createdBy) {
          that.toggleCompleted(null, habit);
        }
      });
      that.disabled = true;
    }, function (error) {
      console.log('STOMP Error occurred: ' + error);
    });
  }

  ngOnInit() {
    this.resetForm();
    this.subscription = this.habitService.getAll().subscribe(data => {
      this.habits = data;
    });

    for (let i = 0; i < 24; i++) {
      this.hours.push({
        value: i,
        viewValue: i
      });
    }

    for (let i = 0; i < 60; i++) {
      this.minutes.push({
        value: i,
        viewValue: i
      });
      this.seconds.push({
        value: i,
        viewValue: i
      });
    }

  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
    this.habitService.selectedHabit = new Habit();
  }

  onSubmit(form: NgForm) {
    if (form.value.id == null) {
      this.habitService.insert(form.value).subscribe(
        (data: Habit) => {
          this.toastr.success('Habit created successfully - ' + data.description);
          const newHabits = this.habits.slice(0);
          newHabits.push(data);
          this.habits = newHabits;
          this.habit = new Habit();
          this.resetForm(form);
          this.modalService.dismissAll();
        }
      );
    } else {
      this.habitService.update(form.value).subscribe(
        (data: Habit) => {
          this.toastr.success('Habit updated successfully - ' + data.description);
          this.changeHabit(data.id, data);
        }
      );
    }

  }

  changeHabit(id, habit: Habit) {
    for (const i in this.habits) {
      if (this.habits[i].id === id) {
        this.habits[i] = habit;
        break;
      }
    }
  }

  insert(habit: Habit) {
    if (!habit.description) {
      alert('description may not be left blank!');
      return;
    }
    if (!habit.hours) {
      const foundHourMatch = habit.description.match(/([0-9]+)\s(hour|hours|hr|hrs)/i);
      if (foundHourMatch) {
        habit.hours = (+foundHourMatch[1]);
      }
    }
    if (!habit.minutes) {
      const foundMinuteMatch = habit.description.match(/([0-9]+)\s(minute|minutes|min|mins)/i);
      if (foundMinuteMatch) {
        habit.minutes = (+foundMinuteMatch[1]);
      }
    }
    if (!habit.seconds) {
      const foundSecondMatch = habit.description.match(/([0-9]+)\s(second|seconds|sec|secs)/i);
      if (foundSecondMatch) {
        console.log(foundSecondMatch[1]);
        habit.seconds = (+foundSecondMatch[1]);
      }
    }

    this.habitService.insert(habit).subscribe(
      (data: Habit) => {
        this.toastr.success('Habit created successfully - ' + data.description);
        const newHabits = this.habits.slice(0);
        newHabits.push(data);
        this.habits = newHabits;
        this.habit = new Habit();
      }
    );
  }

  onAdd(content) {
    this.resetForm();
    this.open(content);
  }

  onDelete(habit: Habit) {
    this.habitService.delete(habit.id);
    this.habits = this.habits
      .filter(function (el) {
        return el.id !== habit.id;
      });
  }

  open(content) {
    this.modalService.open(content);
  }

  toggleCompleted(e, habit: Habit) {
    habit.completed = true;
    this.streakService.insert(habit).subscribe(
      (data: Habit) => {
        this.toastr.success('Habit marked as ' + (data.completed ? 'complete' : 'incomplete'));
        data.completed = false;
        this.changeHabit(habit.id, data);
        console.log(this.habits);
      }
    );
  }

  onStart(habit: Habit) {
    habit.started = true;
    this.habitService.start(habit).subscribe(
      data => console.log(data)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
