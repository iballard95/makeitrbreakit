import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth/AuthService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user;
  constructor(private router: Router,
              private auth: AuthService) { }
  ngOnInit() {
    this.getUser();
  }
  getUser() {
    this.auth.currentUserObservable.subscribe(user => {
      this.auth.user = user;
      this.user = user;
      this.router.navigate(['app']);
    }, (err => {
      console.log(err);
      this.user = null;
    }));
  }
}
