import {Component, OnDestroy, OnInit} from '@angular/core';
import {Chart} from 'chart.js';
import {HabitService} from "../shared/habit/habit.service";


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit, OnDestroy {

  chart = [];
  subscription;
  constructor(public habitService: HabitService) {}

  ngOnInit() {
    this.subscription = this.habitService.getAllStats().subscribe(data => {
      const labels = data.labels;
      const dataSets = [];
      data.contexts.forEach(s => {
        dataSets.push({
          data: s.counts,
          label: s.name,
          color: '#F39E01'
        });
      });
      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: labels,
          datasets: dataSets
        },
        options: {
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
      this.subscription.unsubscribe();
  }
}
