import { Component, OnInit } from '@angular/core';
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {AuthService} from "../auth/AuthService";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router,
              private auth: AuthService) { }
  user;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  ngOnInit() {
      this.user = this.auth.user;
  }

  onLogout() {
    this.auth.logout();
  }

  onStats() {
    this.router.navigate(['app/stats']);
  }


  goHome() {
    this.router.navigate(['app']);
  }

}
