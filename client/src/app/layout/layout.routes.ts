import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './layout.component';
import {HabitsComponent} from "../habits/habits.component";
import {StatsComponent} from "../stats/stats.component";

const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
      { path: 'habits', component: HabitsComponent },
      { path: 'stats', component: StatsComponent },
      { path: '', redirectTo: 'habits', pathMatch: 'full' }
    ]}
];
export const ROUTES = RouterModule.forChild(routes);
