import {ROUTES} from './layout.routes';
import {NgModule} from '@angular/core';
import {LayoutComponent} from './layout.component';
import {HabitsComponent} from '../habits/habits.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StatsComponent} from "../stats/stats.component";

import {
  MatButtonModule,
  MatCardModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatSelectModule
} from '@angular/material';


@NgModule({
  imports: [
    ROUTES,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatMenuModule,
    MatSidenavModule,
    MatIconModule,
    MatSelectModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LayoutComponent,
    HabitsComponent,
    StatsComponent
  ],
  providers: [],
  bootstrap: [LayoutComponent]
})
export class LayoutModule {
}
