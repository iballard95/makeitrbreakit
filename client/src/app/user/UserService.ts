import {User} from "../shared/user/user.model";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) { }

  getCurrentUser() {
    return this.http.get<User>('/user');
  }
}
