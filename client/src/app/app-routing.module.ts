import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from "./auth/AuthGuard";


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'app', loadChildren: './layout/layout.module#LayoutModule' , canActivateChild:[AuthGuard] },
  { path : '', component : LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent
];
