import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../shared/user/user.model';
import {UserService} from '../user/UserService';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from "../../environments/environment";

@Injectable({ providedIn: 'root' })
export class AuthService {

  public user: User;

  constructor(private http: HttpClient,
              private userService: UserService,
              private router: Router) {
  }

  public get currentUserObservable(): Observable<User> {
    return this.userService.getCurrentUser();
  }

  logout() {
    this.user = null;
    this.http.post('/logout', {}, { headers: {responseType: 'text'} }).subscribe( res => {
      this.router.navigate(['']);
    });
  }
}
