import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Habit} from "../habit/habit.model";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StreakService {

  constructor(private http: HttpClient) {

  }

  insert(habit: Habit): Observable<any> {
    return this.http.post('/habits/' + habit.id + '/streak', habit);
  }
}
