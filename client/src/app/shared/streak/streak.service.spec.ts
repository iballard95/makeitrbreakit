import { TestBed } from '@angular/core/testing';

import { StreakService } from './streak.service';

describe('StreakService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StreakService = TestBed.get(StreakService);
    expect(service).toBeTruthy();
  });
});
