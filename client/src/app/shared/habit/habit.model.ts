import {Streak} from './streak/streak.model';

export class Habit {
  id: number;
  description: string;
  completed: boolean;
  streakCount: number;
  started: boolean;
  hours: number;
  minutes: number;
  seconds: number;
  streaks: Streak[];
}
