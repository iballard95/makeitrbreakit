import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Habit} from "./habit.model";
import {ToastrService} from "ngx-toastr";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HabitService {
  selectedHabit: Habit = new Habit();
  constructor(private http: HttpClient,
              private toastr: ToastrService) {
  }

  getAllStats(): Observable<any>  {
    return this.http.get('/stats?numDays=7');
  }


  getAll(): Observable<any>  {
    return this.http.get('/habits');
  }

  insert(habit: Habit): Observable<any> {
    return this.http.post('/habits', habit);
  }

  update(habit: Habit): Observable<any> {
    return this.http.put('/habits/' + habit.id, habit);
  }

  delete(id: number) {
    this.http.delete('/habits/' + id).subscribe(
      (data: Habit) => {
        this.toastr.success('Habit deleted successfully');
      }
    );
  }

  start(habit: Habit) {
    return this.http.post('/timer/start', habit);
  }

}
