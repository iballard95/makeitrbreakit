export class User {
  authenticated: boolean;
  authorities: Authority[];
  details: Details;
  name: string;
  userAuthentication: UserAuthentication;
}

export class Authority {
  authority: string;
}

export class Details {
  remoteAddress: string;
  sessionId: string;
  tokenType: string;
  tokenValue: string;
}

export class UserAuthentication {
  authenticated: boolean;
  authorities: Authority[];
  details: UserAuthenticationDetails;
}

export class UserAuthenticationDetails {
  email: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  hd: string;
  locale: string;
  name: string;
  picture: string;
}
